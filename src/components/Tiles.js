import React, { Component } from 'react';
import { View, Text, Image, StyleSheet } from 'react-native';

export default class Tiles extends Component {

    constructor(props) {
        super(props);
    }

    render = () => {
        return (
            <View style={(this.props.left) ? { marginLeft: 20 } : ''}>
                <Image style={[{
                    backgroundColor: '#FFBFCB',
                    borderRadius: 10,
                    width: 114,
                    height: 120
                },
                (this.props.size == "small") ? styles.small : '',
                (this.props.size == "big") ? styles.big : '']} />
                <Text style={[styles.songNameText, (this.props.textCenter == true) ? {textAlign: 'center'} : '']}>{(this.props.songName) != undefined ? this.props.songName : 'Song Name'}</Text>
                {(this.props.artist) != undefined ? <Text style={styles.artistText}>{this.props.artist}</Text> : <Text></Text> }
            </View>
        );
    }
}

const styles = StyleSheet.create({
    small: {
        width: 114,
        height: 120
    },
    big: {
        width: 180,
        height: 180
    },
    artistText: {
        fontFamily: 'Poppins-Regular',
        fontSize: 13,
        color: 'rgba(0,0,0,0.5)'
    },
    songNameText : {
        fontFamily: "Poppins-SemiBold",
        fontSize: 15,
        color: 'black'
    }
});