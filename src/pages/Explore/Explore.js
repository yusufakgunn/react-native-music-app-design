import React, { Component } from 'react';
import { Text, View, ScrollView, Image, ImageBackground, Dimensions, StyleSheet } from 'react-native';

import Tiles from '../../components/Tiles';

const { width, height } = Dimensions.get('window');

export default class Explore extends Component {

  constructor(props) {
    super(props);
    this.state = { viewRef: null };
  }

  render = () => {
    return (
      <ScrollView stickyHeaderIndices={[1]} style={styles.container}>
        <Text style={{
          fontFamily: "Poppins-Bold",
          fontSize: 34,
          color: 'black',
          paddingLeft: 15
        }}>Explore</Text>
        <View style={styles.recently_played}>
          <Text style={{
            fontFamily: "Poppins-Bold",
            fontSize: 22,
            color: 'black'
          }}>Recently played</Text>
          <ScrollView showsHorizontalScrollIndicator={false} horizontal={true}>
            <Tiles size="small" />
            <Tiles left={true} size="small" />
            <Tiles left={true} size="small" />
            <Tiles left={true} size="small" />
            <Tiles left={true} size="small" />
            <Tiles left={true} size="small" />
          </ScrollView>
        </View>
        <View style={styles.recommended}>
          <Text style={{
            fontFamily: "Poppins-Bold",
            fontSize: 22,
            color: 'black'
          }}>Recommended for you</Text>
          <ScrollView showsHorizontalScrollIndicator={false} horizontal={true}>
            <Tiles size="big" artist="Artist" />
            <Tiles left={true} size="big" artist="Artist" />
            <Tiles left={true} size="big" artist="Artist" />
            <Tiles left={true} size="big" artist="Artist" />
            <Tiles left={true} size="big" artist="Artist" />
            <Tiles left={true} size="big" artist="Artist" />
          </ScrollView>
        </View>
        <View style={styles.inspired}>
          <Text style={{
            fontFamily: "Poppins-Bold",
            fontSize: 22,
            color: 'black'
          }}>Get inspired!</Text>
          <ScrollView showsHorizontalScrollIndicator={false} horizontal={true}>
            <Tiles size="big" artist="Artist" />
            <Tiles left={true} size="big" artist="Artist" />
            <Tiles left={true} size="big" artist="Artist" />
            <Tiles left={true} size="big" artist="Artist" />
            <Tiles left={true} size="big" artist="Artist" />
            <Tiles left={true} size="big" artist="Artist" />
          </ScrollView>
        </View>
      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  recently_played: {
    width: width,
    height: height - 630,
    paddingLeft: 15
  },
  recommended: {
    paddingLeft: 15,
    width: width,
    height: height - 550
  },
  inspired: {
    paddingLeft: 15,
    width: width,
    height: height - 550
  }
});