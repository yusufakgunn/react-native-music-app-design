import React from "react";
import { Dimensions } from "react-native";
import { createAppContainer, createBottomTabNavigator } from 'react-navigation';
import Icon from 'react-native-vector-icons/MaterialIcons';

import Explore from './src/pages/Explore/Explore';
import Trending from './src/pages/Trending/Trending';
import Search from './src/pages/Search/Search';
import Library from './src/pages/Library/Library';
import Settings from './src/pages/Settings/Settings';

const { width, height } = Dimensions.get('window');

const AppNavigator = createBottomTabNavigator({
  ExplorePage: {
    screen: Explore,
    navigationOptions: {
      tabBarLabel: 'Explore',
      tabBarIcon: ({ focused }: any) =>
        focused ? <Icon name="music-note" size={30} style={{ color: '#FF2D55' }} /> : <Icon name="music-note" size={30} style={{ color: '#707070' }} />
    }
  },
  TrendingPage: {
    screen: Trending,
    navigationOptions: {
      tabBarLabel: 'Trending',
      tabBarIcon: ({ focused }: any) =>
        focused ? <Icon name="home" size={30} style={{ color: '#FF2D55' }} /> : <Icon name="home" size={30} style={{ color: '#707070' }} />
    }
  },
  SearchPage: {
    screen: Search,
    navigationOptions: {
      tabBarLabel: 'Search',
      tabBarIcon: ({ focused }: any) =>
        focused ? <Icon name="search" size={30} style={{ color: '#FF2D55' }} /> : <Icon name="search" size={30} style={{ color: '#707070' }} />
    }
  },
  LibraryPage: {
    screen: Library,
    navigationOptions: {
      tabBarLabel: 'Library',
      tabBarIcon: ({ focused }: any) =>
        focused ? <Icon name="library-music" size={30} style={{ color: '#FF2D55' }} /> : <Icon name="library-music" size={30} style={{ color: '#707070' }} />
    }
  },
  SettingsPage: {
    screen: Settings,
    navigationOptions: {
      tabBarLabel: 'Settings',
      tabBarIcon: ({ focused }: any) =>
        focused ? <Icon name="settings" size={30} style={{ color: '#FF2D55' }} /> : <Icon name="settings" size={30} style={{ color: '#707070' }} />
    }
  }
},
  {
    headerMode: 'none',
    initialRouteName: "ExplorePage",
    barStyle: { backgroundColor: 'white' },
    tabBarOptions: {
      activeTintColor: '#FF2D55',
      inactiveTintColor: '#707070',
      labelStyle: {
        fontFamily: 'Poppins-SemiBold',
        fontSize: 12
      },
      tabStyle: {
        paddingVertical: 7
      },
      style: {
        height: height - 750
      }
    },
  }
);

export default createAppContainer(AppNavigator);